# Traffic

AI to identify which speed traffic sign appears in a photograph build for a [cs50AI project](https://cs50.harvard.edu/ai/2020/projects/5/traffic/).

This project uses the [German Traffic Sign Recognition Benchmark (GTSRB)](http://benchmark.ini.rub.de/?section=gtsrb&subsection=news) dataset for images of traffic signs.

# Experimentation Process

After implementing the two basic functions, different properties of the neural network model were changed in order to see the difference in the results. Some of the results and conclusions are documented below. All of the tests were over the GTSRB dataset and used the default paramters:

```python
EPOCHS = 10
IMG_WIDTH = 30
IMG_HEIGHT = 30
NUM_CATEGORIES = 43
TEST_SIZE = 0.4
```

## First Model

The first compiled model was inspired in the [lecture's notes](https://cs50.harvard.edu/ai/2020/notes/5/). It was only adapted to receive the correct input shape and output the correct number of categories.

```python
model = tf.keras.models.Sequential([

    tf.keras.layers.Conv2D(
        32, (3, 3), activation="relu", input_shape=(IMG_WIDTH, IMG_HEIGHT, 3)
    ),

    tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),

    tf.keras.layers.Flatten(),

    tf.keras.layers.Dense(128, activation="relu"),

    tf.keras.layers.Dropout(0.5),

    tf.keras.layers.Dense(NUM_CATEGORIES, activation="softmax")
])

model.compile(
    optimizer="adam",
    loss="categorical_crossentropy",
    metrics=["accuracy"]
)
```

The results were bad and the network could not distinguish the categories.

```bash
# model.evaluate()
333/333 - 0s - loss: 3.4898 - accuracy: 0.0558
```

## Change in Dropout

At first, some of the changes suggested in the project's description were tried, but the first one that made significant difference was reducing the dropout layer value from `0.5` to `0.1`.

With the dropout reduction, the network started to predict the categories with great accuracy and the loss dropped drastically. Comparing to the handwriting recognition, that worked well with a `0.5` dropout, this can be due to the images complexity difference. The handwritten digits were simple when compared to the traffic signs.

```bash
# model.evaluate()
333/333 - 0s - loss: 0.5105 - accuracy: 0.9276
```

## Addition of Dense Layers

After the change in dropout, another dense layer with size `256` was added right before the old one, in an attempt to build a more robust network.

With the addition, the evaluation accuracy increased a little and the loss dropped by almost half.

Since the network has more nodes now, an attempt to increase the dropout again to prevent overfitting was made and an interesting result happened. Some of the runs using a `0.5` dropout never achieved more than `0.06` of accuracy and got stuck. Other runs, however, scaped this mark in the first epoch and got results similar to the `0.1` dropout (which got consistent accuracy).

```bash
# model.evaluate() for 0.1 dropout
333/333 - 1s - loss: 0.2826 - accuracy: 0.9370
```

## Change in Pooling Mask Size

Changes in the `MaxPooling2D` mask size were tried. First, removing the layer (equivalente to a size `(1,1)`) made no significant difference in the accuracy, but raised the loss and the fit process was very slow (as the network got larger).

Using a `(3,3)` size produced similar results to the old `(2,2)` both in accuracy and loss.

Using a `(4,4)` size just made the results worse. The downgrade only happened in the evaluation: during the fit, the results looked good. This is expected, as a large mask simplify the entries too much, making the network good in predicting the category for the images it was trained on, but bad in describing other images (test set).

```bash
# model.evaluate() for no pooling
333/333 - 1s - loss: 0.3966 - accuracy: 0.9357
# model.evaluate() for pooling mask size of (3,3)
333/333 - 1s - loss: 0.2959 - accuracy: 0.9414
# model.evaluate() for pooling mask size of (4,4)
333/333 - 1s - loss: 0.3946 - accuracy: 0.9069
```
